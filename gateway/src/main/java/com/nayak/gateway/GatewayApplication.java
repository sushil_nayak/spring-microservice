package com.nayak.gateway;

import brave.sampler.Sampler;
import com.google.common.util.concurrent.RateLimiter;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import javax.servlet.http.HttpServletResponse;

@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    @Bean
    public Sampler alwaysSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }

}

@Component
class CustomZuulFilter extends ZuulFilter {

    private final com.google.common.util.concurrent.RateLimiter rateLimiter = RateLimiter.create(1.0 / 30.0);

    @Override
    public boolean shouldFilter() {
        return false;
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext currentContext  = RequestContext.getCurrentContext();
        HttpServletResponse response = currentContext .getResponse();

        if (!rateLimiter.tryAcquire()) {
            try {
                response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
                currentContext.setSendZuulResponse(false);
                throw new ZuulException("Couldn't proceed!!",
                        HttpStatus.TOO_MANY_REQUESTS.value(),
                        HttpStatus.TOO_MANY_REQUESTS.getReasonPhrase());
            }
            catch (ZuulException e) {
                ReflectionUtils.rethrowRuntimeException(e);
            }
        }

        return null;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 100;
    }
}