package com.nayak.secondmicroservice;

import brave.sampler.Sampler;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

@EnableHystrix
@EnableDiscoveryClient
@SpringBootApplication
public class SecondMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondMicroserviceApplication.class, args);
    }

    @Bean
    public Sampler alwaysSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }
}

@Configuration
class RestConfig {
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }
}

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@RestController
class MainController {
    private final RestTemplate restTemplate;

    @GetMapping("/empls")
    @HystrixCommand(fallbackMethod = "employeeFallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "4000"),
            @HystrixProperty(name = "execution.timeout.enabled", value = "true")
    })
    public ResponseEntity empls() throws InterruptedException {
        long delay = 1000 * new Random().nextInt(9);
        log.info("Delay was " + delay);
        Thread.sleep(delay);
        return restTemplate.exchange("http://FIRST-SERVICE/api/v1/employees", HttpMethod.GET, null, String.class);
    }

    public ResponseEntity employeeFallback() {
        log.info("Something went wrong");
        return ResponseEntity.ok("Damn!!!. something went wrong..woopsie!!");
    }
}
