package com.nayak.firstmicroservice.controller;

import com.nayak.firstmicroservice.model.Employee;
import com.nayak.firstmicroservice.repository.BasicEmployeeProjection;
import com.nayak.firstmicroservice.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class MainController {
    private final EmployeeRepository employeeRepository;

    @GetMapping("employees")
    public ResponseEntity employees(@RequestParam(value = "size", defaultValue = "10", required = false) Integer size,
                                    @RequestParam(value = "page", defaultValue = "1", required = false) Integer page) {
        Page<Employee> all = employeeRepository.findAll(PageRequest.of(page - 1, size));
        log.info("{}", "hello");
        return ResponseEntity.ok(all);
    }

    @GetMapping("employees-projection")
    public ResponseEntity employee(@RequestParam(value = "size", defaultValue = "10", required = false) Integer size,
                                   @RequestParam(value = "page", defaultValue = "1", required = false) Integer page) {
        Page<BasicEmployeeProjection> byId = employeeRepository.findAllBy(PageRequest.of(page - 1, size));
        log.info("{}", "hello");
        return ResponseEntity.ok(byId);
    }
}
