package com.nayak.firstmicroservice.repository;

import com.nayak.firstmicroservice.model.Address;

public interface BasicEmployeeProjection {
    String getFirstName();

    Address getHomeAddress();

    interface Address {
        String getAddressLine1();
    }

}
