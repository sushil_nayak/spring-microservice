package com.nayak.firstmicroservice.repository;

import com.nayak.firstmicroservice.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface EmployeeRepository extends JpaRepository<Employee, String> {

    Page<BasicEmployeeProjection> findAllBy( Pageable pageable);
}
