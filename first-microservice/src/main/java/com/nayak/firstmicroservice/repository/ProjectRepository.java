package com.nayak.firstmicroservice.repository;

import com.nayak.firstmicroservice.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, String> {

}
