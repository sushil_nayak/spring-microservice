package com.nayak.firstmicroservice.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.List;

@Getter
@Entity
public class FullTimeEmployee extends Employee {
    private double monthSalary;

    public FullTimeEmployee() {
    }

    @Builder
    public FullTimeEmployee(String firstName, Address homeAddress, Address workAddress, List<String> skills, double monthSalary) {
        super(firstName, homeAddress, workAddress, skills);
        this.monthSalary = monthSalary;
    }
}
