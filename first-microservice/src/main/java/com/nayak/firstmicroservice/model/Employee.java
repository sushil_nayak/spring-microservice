package com.nayak.firstmicroservice.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Employee {

    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @GenericGenerator(strategy = "uuid", name = "uuid-strategy")
    String id;


    String firstName;

    @Embedded
    @AttributeOverride(name = "addressLine1", column = @Column(name = "homeAddressLine1"))
    @AttributeOverride(name = "addressLine2", column = @Column(name = "homeAddressLine2"))
    @AttributeOverride(name = "addressLine3", column = @Column(name = "homeAddressLine3"))
    Address homeAddress;

    @Embedded
    @AttributeOverride(name = "addressLine1", column = @Column(name = "workAddressLine1"))
    @AttributeOverride(name = "addressLine2", column = @Column(name = "workAddressLine2"))
    @AttributeOverride(name = "addressLine3", column = @Column(name = "workAddressLine3"))
    Address workAddress;

    @ElementCollection
    @CollectionTable(name = "employee_skill", joinColumns = @JoinColumn(name = "skill_id"))
    List<String> skills = new ArrayList<>();

    @ManyToMany(mappedBy = "employees",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.EAGER)
    List<Project> projects = new ArrayList<>();

    public Employee(String firstName, Address homeAddress, Address workAddress, List<String> skills) {
        this.firstName = firstName;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
        this.skills = skills;
    }

}
