package com.nayak.firstmicroservice.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.util.List;


@Getter
@Entity
public class PartTimeEmployee extends Employee {
    private double hourlyWage;

    public PartTimeEmployee() {
    }

    @Builder
    public PartTimeEmployee(String firstName, Address homeAddress, Address workAddress, List<String> skills, double hourlyWage) {
        super(firstName, homeAddress, workAddress, skills);
        this.hourlyWage = hourlyWage;
    }
}
