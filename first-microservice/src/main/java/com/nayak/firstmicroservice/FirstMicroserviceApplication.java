package com.nayak.firstmicroservice;

import brave.sampler.Sampler;
import com.nayak.firstmicroservice.model.*;
import com.nayak.firstmicroservice.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import javax.transaction.Transactional;
import java.util.Arrays;

@EnableEurekaClient
@SpringBootApplication
public class FirstMicroserviceApplication implements CommandLineRunner {
    @Autowired
    ProjectRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(FirstMicroserviceApplication.class, args);
    }

    @Bean
    public Sampler alwaysSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {

        Address homeAddress = Address.builder().addressLine1("zzzz").addressLine2("vvvv").addressLine3("aaaa").build();
        Address workAddress = Address.builder().addressLine1("qqqq").addressLine2("uuuu").build();
//        Employee employee=new FullTimeEmployee("Sushil", homeAddress, workAddress, Arrays.asList("java", "scala"), Arrays.asList("ms-1"))
        Employee employee = FullTimeEmployee.builder()
                .firstName("Sushil")
                .homeAddress(homeAddress)
                .workAddress(workAddress)
//                .projects(Arrays.asList(new Project("secret-ms")))
                .skills(Arrays.asList("java", "scala"))
                .monthSalary(3000)
                .build();
        Project project = new Project("secret-ms", Arrays.asList(employee));

        Employee kangan = PartTimeEmployee.builder()
                .firstName("Kangan")
                .homeAddress(homeAddress)
                .workAddress(workAddress)
//                .projects(Arrays.asList(new Project("etl-service")))
                .skills(Arrays.asList("python"))
                .hourlyWage(400.00)
                .build();
        Project etlService = new Project("etl-service", Arrays.asList(kangan));

        repository.save(project);
        repository.save(etlService);
    }
}

